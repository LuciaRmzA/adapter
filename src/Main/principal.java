package Main;

import MotorAutomotiz.MotorComun;
import MotorAutomotiz.MotorEconomico;
import MotorAutomotiz.MotorNormal;
import NuevoMotor.MotorElectrico;
import NuevoMotor.NuevoMotor;
import adapter.MotorModernoAdapter;
import adapter.MotorNormalAdapter;

public class principal {
	public static void main(String[] args) {
		
		System.out.println("\tSandra Lucia Ramirez Avila - Proyecto adaptador");
		System.out.println("--------------------------------------------------------");
		System.out.println("MotorNormal...");
		MotorComun mc = new MotorComun();
		mc.encender();
		mc.acelerar();
		mc.Apagar();
		
		System.out.println("\n\nMotorElectronico...");
		MotorElectrico mee = new MotorElectrico();
		mee.Conectar();
		mee.Activar();
		mee.moverRapido();
		mee.detener();
		mee.desconectar();

		System.out.println("\n\nMotorNormal adapater MotorElectrico...");
		MotorElectrico motorElectrico = new MotorElectrico();
		MotorNormal Adapter1 = new MotorModernoAdapter(motorElectrico);
		Adapter1.encender();
		Adapter1.acelerar();
		Adapter1.Apagar();
		
		System.out.println("\n\nMotorElectrico adapater MotorNormal...");
		MotorComun motorComun = new MotorComun();
		NuevoMotor Adapter2 = new MotorNormalAdapter(motorComun);
		Adapter2.Conectar();
		Adapter2.Activar();
		Adapter2.moverRapido();
		Adapter2.detener();
		Adapter2.desconectar();

	}

}
