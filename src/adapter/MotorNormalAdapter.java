package adapter;

import MotorAutomotiz.MotorNormal;
import NuevoMotor.NuevoMotor;

public class MotorNormalAdapter implements NuevoMotor {
	
	MotorNormal mn;
	
	public MotorNormalAdapter(MotorNormal MotorNormal){
		this.mn = MotorNormal;
	}

	@Override
	public void Conectar() {
		Activar();
	}

	@Override
	public void Activar() {
		mn.encender();	
	}

	@Override
	public void moverRapido() {
		mn.acelerar();
	}

	@Override
	public void detener() {
		desconectar();
	}

	@Override
	public void desconectar() {
		mn.Apagar();
		
	}

}
