package adapter;

import MotorAutomotiz.MotorNormal;
import NuevoMotor.NuevoMotor;

public class MotorModernoAdapter implements  MotorNormal{
	
	NuevoMotor mm;
	
	public MotorModernoAdapter(NuevoMotor MotorModern){
		this.mm = MotorModern;
	}

	@Override
	public void encender() {
		mm.Conectar();
		mm.Activar();		
	}

	@Override
	public void acelerar() {
		mm.moverRapido();
	}

	@Override
	public void Apagar() {
		mm.detener();
		mm.desconectar();
		
	}

}
