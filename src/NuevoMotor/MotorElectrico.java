package NuevoMotor;

public class MotorElectrico  implements NuevoMotor{

	@Override
	public void Conectar() {
		System.out.println("Se conecto carro.");
	}

	@Override
	public void Activar() {
		System.out.println("Carro activado.");
	}

	@Override
	public void moverRapido() {
		System.out.println("Acelero el carro.");
	}

	@Override
	public void detener() {
		System.out.println("Se detuvo carro.");
	}

	@Override
	public void desconectar() {
		System.out.println("Se desconecto carro.");	
	}

}
